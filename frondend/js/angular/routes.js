app.config(["$routeProvider", "$locationProvider", function($routeProvider, $locationProvider){

    $routeProvider
    .when("/", {
        templateUrl: "/frondend/views/home.html",
        controller: "HomeController"
        // page_title: "Αρχική",
        // name: "home",
        // resolve: {
        //     tokenExists: tokenExists,
        //     checkRoute: checkRoute
        // }
    })
    .when("/testme", {
        templateUrl: "/frondend/views/testme.html"        
    })
    .otherwise({
        templateUrl: "/frondend/views/404.html"
    });


    $locationProvider.html5Mode(true);


}]);